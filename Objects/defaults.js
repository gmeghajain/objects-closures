// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
function defaults(obj, defaultProps) {
    if(!obj){
        if(!defaultProps) return {};
        else {
            obj=defaultProps;
            return obj;
        }
    }
    for (let prop in defaultProps) {
        if (!obj.hasOwnProperty(prop)) {
          obj[prop] = defaultProps[prop];
        }
    }
    return obj;
}
module.exports = defaults;