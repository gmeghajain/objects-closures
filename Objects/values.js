// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj) {
    if(!obj) return;
    const objValues = Object.values(obj);
    return objValues;
}

module.exports = values;