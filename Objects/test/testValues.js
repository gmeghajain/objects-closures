// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

const testObject = require("../testObject.js")
const values = require("../values.js")

const ans = values(testObject);
console.log(ans);