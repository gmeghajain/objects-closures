// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert
const invert = require("../invert")
const testObject = require("../testObject")

let ans = invert(testObject);
console.log("Sent Object : ");
console.log(testObject);
console.log("Inverted Object : ");
console.log(ans);