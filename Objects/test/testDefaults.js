// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
const defaults = require("../defaults")

const defaultProps={name: "Harry", surname: "Potter", school: "Hogwarts", game: "Quidditch"};
console.log("Default object properties : " );
console.log(defaultProps);


const obj={name: "Harry",school: "Hogwarts",wand: "11'"};
console.log("Original obj : ");
console.log(obj);

const ans=defaults(obj,defaultProps);
console.log("New Object : ");
console.log(ans);
