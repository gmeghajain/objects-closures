// Like map for arrays, but for objects.
//Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

const testObject = require("../testObject")
const mapObject = require("../mapObject")

const cb = (val, key) => {
    return val + "!";
  };
const result = mapObject(testObject, cb);
console.log(result);
    
