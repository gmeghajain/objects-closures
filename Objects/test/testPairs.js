// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

const testObj=require("../testObject")
const pairs=require("../pairs")

const ans = pairs(testObj);
console.log(ans);