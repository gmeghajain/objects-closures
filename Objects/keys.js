// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

//const testObject = require testObject.js
function keys(obj) {
    if(!obj) return;
    const objPropertyNames = Object.keys(obj);
    return objPropertyNames;    
}
module.exports = keys;