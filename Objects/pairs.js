// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj) {
    if(!obj) return;
    //const pair=Object.pairs(obj);
    const pair=Object.entries(obj);
    return pair;   
}

module.exports = pairs;