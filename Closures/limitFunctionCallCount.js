// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
    if(n==undefined || cb==undefined) return () => NaN;
    let counter=0;
    return () =>
    {
        if(counter<n)
        {
            counter++;
            return cb();
        }
        return null;
    };    
}
module.exports=limitFunctionCallCount;